<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\RequestException;

class JsonRpcClient
{
    const RPC_VERSION = '2.0';

    /**
     * @var Http
     */
    protected $client;

    /**
     * JsonRpcClient constructor.
     */
    public function __construct()
    {
        $this->client = Http::contentType('application/json');
    }

    /**
     * @param string $method
     * @param array  $params
     * @return array
     * @throws RequestException
     */
    public function send(string $method, array $params = []): array
    {
        return $this->client->post(config('services.api.base_url'), [
            'jsonrpc' => self::RPC_VERSION,
            'method'  => $method,
            'params'  => $params,
            'id'      => time(),
        ])->throw()->json();
    }
}
