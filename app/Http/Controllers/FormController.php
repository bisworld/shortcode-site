<?php

namespace App\Http\Controllers;

use App\Services\JsonRpcClient;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\FormStoreRequest;
use Illuminate\Contracts\Support\Renderable;

class FormController extends Controller
{
    const
        METHOD_INDEX = 'form_index',
        METHOD_STORE = 'form_store',
        METHOD_SHOW  = 'form_show'
    ;

    /**
     * @var JsonRpcClient
     */
    protected $service;

    /**
     * FormController constructor.
     *
     * @param JsonRpcClient $service
     */
    public function __construct(JsonRpcClient $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index()
    {
        $data = collect($this->service->send(self::METHOD_INDEX));

        if (! $data->has('result')) {
            abort(500, 'Server error!');
        }

        $forms = collect($data->get('result'))
            ->map(function (array $form) {
                return collect($form);
            });

        return view('forms.index', compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create()
    {
        return view('forms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FormStoreRequest $request
     * @return RedirectResponse
     */
    public function store(FormStoreRequest $request)
    {
        $data = collect(
            $this->service->send(self::METHOD_STORE, $request->validated())
        );

        if (! $data->has('result')) {
            abort(500, 'Server error');
        }

        return response()->redirectToRoute('forms.index')->with('success', 'Form Stored!');
    }

    /**
     * Display the specified resource.
     *
     * @param string $page_uid
     * @return Renderable
     */
    public function show(string $page_uid)
    {
        $data = collect(
            $this->service->send(self::METHOD_SHOW, ['page_uid' => $page_uid])
        );

        if (! $data->has('result')) {
            abort(404, 'Form not found!');
        }

        return view('forms.show', ['form' => collect($data->get('result'))]);
    }
}
