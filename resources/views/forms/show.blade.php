@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Show Form</div>

                <div class="card-body">
                    <div class="form-row">
                        <div class="col">
                            <label for="page_uid" class="sr-only">Page UID</label>
                            <input type="text" class="form-control" id="page_uid" name="page_uid"
                                   value="{{ $form->get('page_uid') }}" readonly>
                        </div>
                        <div class="col">
                            <label for="name" class="sr-only">Name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   value="{{ $form->get('name') }}" readonly>
                        </div>
                    </div>

                    <div class="form-row mt-3">
                        <div class="col">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   value="{{ $form->get('email') }}" readonly>
                        </div>
                        <div class="col">
                            <label for="phone" class="sr-only">Phone</label>
                            <input type="tel" class="form-control" id="phone" name="phone"
                                   value="{{ $form->get('phone') }}" readonly>
                        </div>
                    </div>

                    <div class="form-row  mt-3">
                        <div class="col">
                            <a href="{{ URL::previous() }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
