@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Forms</div>

                <a href="{!! route('forms.create') !!}" class="btn btn-success col-md-2 mx-sm-3 mt-3">Create</a>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Page UID</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($forms as $form)
                            <tr>
                                <td>{{ $form->get('page_uid') }}</td>
                                <td>{{ $form->get('name') }}</td>
                                <td>{{ $form->get('phone') }}</td>
                                <td>{{ $form->get('email') }}</td>
                                <td>
                                    <a href="{!! route('forms.show', [$form->get('page_uid')]) !!}"
                                       class="btn btn-primary">Show</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">
                                    Forms not found
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

