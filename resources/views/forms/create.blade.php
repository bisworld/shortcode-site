@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Store Form</div>

                <div class="card-body">
                    <form method="post" action="{!! route('forms.store') !!}">
                        <div class="form-row">
                            <div class="col">
                                <label for="page_uid" class="sr-only">Page UID</label>
                                <input type="text" class="form-control" id="page_uid" name="page_uid"
                                       placeholder="Enter page_uid" value="{{ request('page_uid', uniqid()) }}"
                                       required>
                            </div>
                            <div class="col">
                                <label for="name" class="sr-only">Name</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Enter name"
                                       value="{{ request('name') }}" required>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="col">
                                <label for="email" class="sr-only">Email</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Enter email" value="{{ request('name') }}" required>
                            </div>
                            <div class="col">
                                <label for="phone" class="sr-only">Phone</label>
                                <input type="tel" class="form-control" id="phone" name="phone"
                                       placeholder="Enter phone" value="{{ request('name') }}" required>
                            </div>
                        </div>

                        <div class="form-row  mt-3">
                            <div class="col">
                                <button type="submit" class="btn btn-success">Store</button>
                                <a href="{{ URL::previous() }}" class="btn btn-primary  ml-1">Back</a>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
