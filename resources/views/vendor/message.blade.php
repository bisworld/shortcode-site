@if (session()->has('errors'))
    <div class="alert alert-danger">
        <ul class="list">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif (session()->has('error'))
    <div class="alert alert-danger">
        <span>Error! {{ session('error') }}</span>
    </div>
@elseif (session()->has('success'))
    <div class="alert alert-success">
        <span>Success! {{ session('success') }}</span>
    </div>
@endif

